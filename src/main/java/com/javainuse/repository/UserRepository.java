package com.javainuse.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.User;


@Repository
public interface UserRepository extends CrudRepository<User,Integer> {

	List<User> findAll();
	User findById(int id);
    User findByPseudo(String pseudo);
	User save(User user);
	void deleteAll();
	void deleteById(int id); 
}
