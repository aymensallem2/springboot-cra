package com.javainuse.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Conges;
import com.javainuse.model.User;
import com.javainuse.repository.CongesRepository;
import com.javainuse.repository.UserRepository;

@RestController
@CrossOrigin(origins = "*")
public class CongesController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	CongesRepository congesRepoistory;
	@Autowired
	UserRepository userRepository;

	@PostMapping("/addConges")
	public Conges add(@RequestBody Conges conges) throws NoSuchAlgorithmException {

		return congesRepoistory.save(conges);

	}

	@GetMapping("/AllConges")
	public List<Conges> allProjects() {
		return congesRepoistory.findAll();
	}

	@GetMapping("/conges/{id}")
	public Conges show(@PathVariable String id) {
		int idconges = Integer.parseInt(id);
		return congesRepoistory.findById(idconges);
	}
	
	@GetMapping("/Userconges/{id}")
	public List<Conges> Userconges(@PathVariable String id) {
		int iduser = Integer.parseInt(id);
		User user = userRepository.findById(iduser);
		return congesRepoistory.findByUser(user);
	}

	@GetMapping("/deleteconges/{id}")
	public void delete(@PathVariable String id) {
		int idconges = Integer.parseInt(id);
		congesRepoistory.deleteById(idconges);
	}

	@PostMapping("/updateconges")
	public ResponseEntity<?> update(@RequestBody Conges Conges) throws NoSuchAlgorithmException {

		if ( Conges.getIdconge() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			return ResponseEntity.ok(congesRepoistory.save(Conges));
		}

	}
}
