package com.javainuse.model;

import java.io.Serializable;
import java.security.Timestamp;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name="tache")
public class Tache implements Serializable{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int idtache;
	
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="idaffectation")
	Affectation affectation;
	
	java.time.Instant date_tache;

	public Tache(int idtache, Affectation affectation, java.time.Instant date_tache) {
		super();
		this.idtache = idtache;
		this.affectation = affectation;
		this.date_tache = date_tache;
	}
	
	public Tache() {

	}

	public int getIdtache() {
		return idtache;
	}

	public void setIdtache(int idtache) {
		this.idtache = idtache;
	}
	
	//@JsonProperty(access = Access.WRITE_ONLY)
	public Affectation getAffectation() {
		return affectation;
	}

	public void setAffectation(Affectation affectation) {
		this.affectation = affectation;
	}

	public java.time.Instant getDate_tache() {
		return date_tache;
	}

	public void setDate_tache(java.time.Instant date_tache) {
		this.date_tache = date_tache;
	}
	
	@PreRemove
	private void removeTachesFromAffectation() {
		//System.out.println("preremove tache");
		affectation.getTaches().remove(this);
		
	 
	}

	

	@Override
	public String toString() {
		return "Tache [idtache=" + idtache + ", affectation=" + affectation.getIdaffectation() + ", date_tache=" + date_tache + "]";
	}
	
	}

